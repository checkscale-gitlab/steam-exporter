package steam

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
)

type steamPlayers struct {
	Response struct {
		Players []steamPlayer `json:"players"`
	} `json:"response"`
}

type steamPlayer struct {
	Steamid                  string `json:"steamid"`
	Communityvisibilitystate int    `json:"communityvisibilitystate"`
	Profilestate             int    `json:"profilestate"`
	Personaname              string `json:"personaname"`
	Commentpermission        int    `json:"commentpermission,omitempty"`
	Profileurl               string `json:"profileurl"`
	Avatar                   string `json:"avatar"`
	Avatarmedium             string `json:"avatarmedium"`
	Avatarfull               string `json:"avatarfull"`
	Lastlogoff               int    `json:"lastlogoff"`
	Personastate             int    `json:"personastate"`
	Realname                 string `json:"realname,omitempty"`
	Primaryclanid            string `json:"primaryclanid"`
	Timecreated              int    `json:"timecreated"`
	Personastateflags        int    `json:"personastateflags"`
	Loccountrycode           string `json:"loccountrycode,omitempty"`
	Locstatecode             string `json:"locstatecode,omitempty"`
	Loccityid                int    `json:"loccityid,omitempty"`
}

func GetPlayerSummaries(key string, steamids []string) []steamPlayer {
	// Prepare the Steam API Call
	log.Info(fmt.Sprintf("Checking the friends of the steamid: %s", steamids))
	steamIdList := strings.Join(steamids, ",")
	playerURL := fmt.Sprintf(steamPlayerApi, key, steamIdList)
	playerAPIResponse := steamPlayers{}

	// Execute the Steam API Call
	err := getJSON(playerURL, &playerAPIResponse)

	// HTTP Error
	if err != nil {
		log.Fatal(err.Error())
	}

	// Remove Players that have a private profile
	playerArray := playerAPIResponse.Response.Players
	var sanitizedPlayerArray []steamPlayer
	for i := 0; i < len(playerArray); i++ {
		if playerArray[i].Communityvisibilitystate == 3 {
			log.Debug("Keeping Player %s", playerArray[i].Steamid)
			sanitizedPlayerArray = append(sanitizedPlayerArray, playerArray[i])
		} else {
			log.Debug(fmt.Sprintf("Removing Player %s", playerArray[i].Steamid))
		}
	}
	return sanitizedPlayerArray
}
