package main

import (
	"net/http"
	"os"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"

	"gitlab.com/PatrickDomnick/steam-exporter/pkg/collector"
	"gitlab.com/PatrickDomnick/steam-exporter/pkg/steam"
)

func main() {
	// Configure Logger
	log.SetReportCaller(true)
	// Get environment variables for Steam
	key, keyPresent := os.LookupEnv("KEY")
	if !keyPresent {
		log.Fatal("STEAM KEY NOT SET")
	}
	steamid, steamidPresent := os.LookupEnv("STEAMID")
	steamids, steamidsPresent := os.LookupEnv("STEAMIDS")
	// Final Steam ID Array
	var steamidArray []string
	if steamidsPresent {
		steamidArray = strings.Split(steamids, ",")
	}

	// Create a List of all Steam IDs
	if steamidPresent {
		friendIds := steam.GetFriendSteamIds(key, steamid)
		// Add all Friends to Array
		steamidArray = append(steamidArray, friendIds...)
		// Add self to Array
		steamidArray = append(steamidArray, steamid)
	}
	log.Debug(steamidArray)

	// Create a new instance of the foocollector and register it with the prometheus client.
	log.Info("Starting the Collector")
	colllector := collector.NewSteamProfile(key, steamidArray)
	prometheus.MustRegister(colllector)

	// This section will start the HTTP server and expose any metrics on the /metrics endpoint.
	http.Handle("/metrics", promhttp.Handler())
	log.Info("Beginning to serve on port :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
